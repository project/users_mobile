<?php

/**
 * @file
 */

/**
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
function users_mobile_admin_form($form, &$form_state) {

  $form['settings'] = array(
    '#type' => 'fieldset',
    '#collapsible' => TRUE,
    '#collapsed' => FALSE,
    '#title' => 'Net-core SMS Configuration',
    '#prefix' => '<div class="settings">',
    '#suffix' => '</div>',
  );

  $form['settings']['sender_id'] = array(
    '#type' => 'textfield',
    '#title' => t('SENDER_ID'),
    '#required' => TRUE,
    '#description' => '',
    '#default_value' => variable_get('sender_id', ''),
  );
  $form['settings']['netcore_sms_sender_id'] = array(
    '#type' => 'textfield',
    '#title' => t('NETCORE_SMS_SENDER_ID'),
    '#required' => TRUE,
    '#description' => '',
    '#default_value' => variable_get('netcore_sms_sender_id', ''),
  );
  $form['settings']['netcore_otp_user_name'] = array(
    '#type' => 'textfield',
    '#title' => t('NETCORE_OTP_USER_NAME'),
    '#required' => TRUE,
    '#description' => '',
    '#default_value' => variable_get('netcore_otp_user_name', ''),
  );
  $form['settings']['netcore_otp_user_password'] = array(
    '#type' => 'textfield',
    '#title' => t('NETCORE_OTP_PASSWORD'),
    '#required' => TRUE,
    '#description' => '',
    '#default_value' => variable_get('netcore_otp_user_password', ''),
  );

  $form['settings']['users_mobile_otp_expiry_time'] = array(
    '#type' => 'select',
    '#title' => t('OTP Expiry Time'),
    '#options' => array(
      30 => '30 seconds',
      60 => '60 seconds',
      120 => '120 seconds',
      180 => '180 seconds',
      300 => '300 seconds',
    ),
    '#required' => TRUE,
    '#description' => t('Set OTP(One Time Password) expiry time in seconds to use for login and register user via mobile number'),
    '#default_value' => variable_get('users_mobile_otp_expiry_time', 30),
  );

  return system_settings_form($form);
}
